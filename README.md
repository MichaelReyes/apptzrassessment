# Michael Reyes assessment exam

## Dependency Injection
Using dagger2 for dependency injection <br />
[Scopes] <br />
AppScope - For application level <br />
ActivityScope - For activity level <br />
FragmentScope - For fragment level <br />
[Modules] <br />
ActivityBuilderModule - For activity mapping <br />
FragmentBuilderModule - For fragment mapping <br />
AppModule - To provide App instance <br />
NetworkModule - To setup and provide retrofit instance <br />
ViewModelModule - For ViewModel mapping <br />

## API calls
Using RxJava for handling calls asynchronously <br />
[Using Repository pattern] <br />
SearchService - api service class <br />
SearchRepository - to pull/call data from service <br />

## Design pattern
Using MVVM for the design pattern v

## Extensions
Using custom extensions <br />
Activity.kt - For activity related extensions (e.g. goToActivity()) <br />
Context.kt - For context related extensions (e.g. showConfirmationMaterialDialog()) <br />
Fragment.kt - For activity related extensions (e.g. goToActivity() using the fragment instance) <br />
Lifecycle.kt - For Lifecycle related extension (e.g. observe()) <br />
Observer.kt - For Rx observer related extension (e.g. ioMainToSubscribe()) <br />

## Flow
App will check if using phone or tablet by checking layout. <br />
If it's a tablet (using twoPane as variable), it will show list(MoviesFragment) side by side with the details(MovieDetailsFragment). <br />
In the layout, if it's a tablet(twoPane = true), list_movies(layout-w900dp) will contain recyclerview side by side with a nav host fragment. If it's a phone(twoPane = false), list_movies(layout) will only contain recyclerview <br />
When adapter item is clicked, if twoPane = true, will call nav host fragment to navigate to MovieDetailsFragment else if twoPane = false, will call MovieDetailsActivity. <br />
Using shared preference to save last Movie that the user is viewing the detail using the key PREF_KEY_DETAILS_VIEWING, if the app closed/killed/crashed, when the app opened again, it will check shared preference for PREF_KEY_DETAILS_VIEWING value. <br />