package com.technologies.apptzrassessment.core.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import io.reactivex.disposables.CompositeDisposable
import retrofit2.HttpException

abstract class BaseViewModel : ViewModel() {

    protected val disposable = CompositeDisposable()

    private val _hasInternetConnection = MutableLiveData(false)
    val hasInternetConnection: LiveData<Boolean> = _hasInternetConnection
    fun setHasInternetConnection(value: Boolean){
        _hasInternetConnection.value = value
    }

    private val _loading = MutableLiveData(false)
    val loading: LiveData<Boolean> = _loading
    fun setLoading(value: Boolean) {
        _loading.value = value
    }

    private val _error = MutableLiveData<String>(null)
    val error: LiveData<String> = _error
    fun setError(value: String?) {
        _error.value = value
    }

    protected fun handleException(exception: Throwable, errorMessage: (String?) -> Unit) {
        if (exception is HttpException) {
            errorMessage.invoke(exception.response()?.errorBody()?.string())
        } else
            errorMessage.invoke(exception.localizedMessage)
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}