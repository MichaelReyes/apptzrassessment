package com.technologies.apptzrassessment.core.extension

import android.content.Context
import com.afollestad.materialdialogs.MaterialDialog

fun Context.showConfirmationMaterialDialog(title: String, message: String, positiveLabel: String, negativeLabel: String, positiveCallback: () -> Unit, negativeCallback: (() -> Unit)? = null){
    MaterialDialog(this).show {
        title(text = title)
        message(text = message)
        positiveButton(text = positiveLabel){
            positiveCallback()
            it.dismiss()
        }
        negativeButton(text = negativeLabel){
            negativeCallback?.invoke()
            it.dismiss()
        }
    }
}