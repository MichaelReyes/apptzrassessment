package com.technologies.apptzrassessment.core.network.repository

import com.technologies.apptzrassessment.core.data.generic.SearchResponse
import com.technologies.apptzrassessment.core.data.movie.Movie
import com.technologies.apptzrassessment.core.network.service.SearchService
import com.technologies.apptzrassessment.core.utils.Constants
import io.reactivex.Single
import javax.inject.Inject

class SearchRepository @Inject constructor(private val service: SearchService) {

    fun getMovies(country: String): Single<SearchResponse<Movie>> {
        return service.getMovies(Constants.PARAM_TERM_STAR, country, Constants.PARAM_MEDIA_MOVIE)
    }


}