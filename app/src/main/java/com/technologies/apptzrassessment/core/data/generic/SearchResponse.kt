package com.technologies.apptzrassessment.core.data.generic

data class SearchResponse<T>(
    val resultCount: Int,
    val results: List<T>
)