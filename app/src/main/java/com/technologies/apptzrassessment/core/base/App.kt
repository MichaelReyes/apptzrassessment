package com.technologies.apptzrassessment.core.base

import android.net.NetworkInfo
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork
import com.jakewharton.rxrelay2.BehaviorRelay
import com.preference.PowerPreference
import com.technologies.apptzrassessment.core.di.component.DaggerApplicationComponent
import com.technologies.apptzrassessment.core.utils.Constants
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers


class App : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerApplicationComponent.factory().create(this)
    }

    override fun onCreate() {
        super.onCreate()
        PowerPreference.init(applicationContext)
        initNetworkObserver()
    }

    private val disposables = CompositeDisposable()

    private fun initNetworkObserver() {

        ReactiveNetwork
            .observeNetworkConnectivity(applicationContext)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                internetConnectionStream.accept(it.available() && it.state() == NetworkInfo.State.CONNECTED)
                PowerPreference.getDefaultFile().putBoolean(
                    Constants.PREF_KEY_NETWORK_CONNECTED,
                    it.available() && it.state() == NetworkInfo.State.CONNECTED
                )
            }.addTo(disposables)
    }

    val internetConnectionStream = BehaviorRelay.create<Boolean>()

}