package com.technologies.apptzrassessment.core.network

import com.technologies.apptzrassessment.core.data.generic.SearchResponse
import com.technologies.apptzrassessment.core.data.movie.Movie
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface AppApi {

    interface Search {
        @GET("search?all")
        fun getMovies(
            @Query("term") term: String,
            @Query("country") country: String,
            @Query("media") media: String
        ): Single<SearchResponse<Movie>>
    }

}