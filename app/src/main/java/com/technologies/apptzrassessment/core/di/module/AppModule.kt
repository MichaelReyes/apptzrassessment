package com.technologies.apptzrassessment.core.di.module

import android.content.Context
import com.technologies.apptzrassessment.core.base.App
import com.technologies.apptzrassessment.core.di.scope.AppScope
import dagger.Module
import dagger.Provides

@Module
class AppModule {

    @Provides
    @AppScope
    fun provideContext(
        app: App
    ): Context {
        return app
    }

}