
package com.technologies.apptzrassessment.core.utils

class Constants {
    companion object{
        const val PREF_KEY_NETWORK_CONNECTED = "_pref_network_connected"
        const val PREF_KEY_DETAILS_VIEWING = "_pref_details_viewing"

        const val PARAM_TERM_STAR = "star"
        const val PARAM_MEDIA_MOVIE = "movie"

    }
}