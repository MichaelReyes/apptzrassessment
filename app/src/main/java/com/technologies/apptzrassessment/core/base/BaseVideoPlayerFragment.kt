package com.technologies.apptzrassessment.core.base

import android.media.session.PlaybackState
import android.net.Uri
import android.os.Bundle
import androidx.databinding.ViewDataBinding
import com.google.android.exoplayer2.DefaultLoadControl
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.technologies.apptzrassessment.R

abstract class BaseVideoPlayerFragment<V : ViewDataBinding> : BaseFragment<V>(),
    Player.EventListener {

    private val exoPlayer: ExoPlayer by lazy(mode = LazyThreadSafetyMode.NONE) {
        val trackSelector = DefaultTrackSelector()
        val loadControl = DefaultLoadControl()

        ExoPlayerFactory.newSimpleInstance(
            context, trackSelector, loadControl
        )
    }

    abstract fun getPlayerView(): PlayerView?

    fun playTrack(url: String) {
        getPlayerView()?.let { playerView ->
            val userAgent = Util.getUserAgent(context, context?.getString(R.string.app_name))

            exoPlayer.prepare(
                HlsMediaSource.Factory(DefaultHttpDataSourceFactory(userAgent))
                    .createMediaSource(Uri.parse(url))
            )

            exoPlayer.playWhenReady = true
            playerView.player = exoPlayer
            exoPlayer.addListener(this)

        }
    }

    override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
        when (playbackState) {
            PlaybackState.STATE_CONNECTING -> {
                showLoading(true)
            }
            PlaybackState.STATE_PLAYING -> {
                showLoading(false)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        exoPlayer.apply {
            stop()
            release()
        }
    }
}