package com.technologies.apptzrassessment.core.network.service


import com.technologies.apptzrassessment.core.data.generic.SearchResponse
import com.technologies.apptzrassessment.core.data.movie.Movie
import com.technologies.apptzrassessment.core.di.scope.AppScope
import com.technologies.apptzrassessment.core.network.AppApi
import io.reactivex.Single
import retrofit2.Retrofit
import javax.inject.Inject

@AppScope
class SearchService
@Inject constructor(
    private val retrofit: Retrofit
) : AppApi.Search {

    private val api by lazy { retrofit.create(AppApi.Search::class.java) }

    override fun getMovies(
        term: String,
        country: String,
        media: String
    ): Single<SearchResponse<Movie>> = api.getMovies(term, country, media)
}
