package com.technologies.apptzrassessment.core.di.module

import com.technologies.apptzrassessment.core.di.scope.FragmentScope
import com.technologies.apptzrassessment.feature.dashboard.details.MovieDetailsFragment
import com.technologies.apptzrassessment.feature.dashboard.movies.MoviesFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilderModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun bindMoviesFragment(): MoviesFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun bindMovieDetailsFragment(): MovieDetailsFragment

}