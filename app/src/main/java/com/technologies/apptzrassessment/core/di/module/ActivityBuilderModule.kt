package com.technologies.apptzrassessment.core.di.module

import com.technologies.apptzrassessment.core.di.scope.ActivityScope
import com.technologies.apptzrassessment.feature.dashboard.DashboardActivity
import com.technologies.apptzrassessment.feature.dashboard.details.MovieDetailsActivity
import com.technologies.apptzrassessment.feature.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun bindSplashActivity(): SplashActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun bindDashboardActivity(): DashboardActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun bindMovieDetailsActivity(): MovieDetailsActivity

}