package com.technologies.apptzrassessment.core.extension

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

fun <T> Single<T>.ioToMainSubscribe(onSubscribe: (Disposable) -> Unit, onSuccess: (T) -> Unit, onError: (Throwable) -> Unit): Disposable {
    return this.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .doOnSubscribe { onSubscribe.invoke(it) }
        .subscribe(
            { onSuccess.invoke(it) },
            { onError.invoke(it) }
        )
}