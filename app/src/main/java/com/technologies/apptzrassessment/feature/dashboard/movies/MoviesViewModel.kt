package com.technologies.apptzrassessment.feature.dashboard.movies

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.technologies.apptzrassessment.R
import com.technologies.apptzrassessment.core.base.BaseViewModel
import com.technologies.apptzrassessment.core.data.movie.Movie
import com.technologies.apptzrassessment.core.extension.ioToMainSubscribe
import com.technologies.apptzrassessment.core.network.repository.SearchRepository
import javax.inject.Inject

class MoviesViewModel
@Inject constructor(
    private val context: Context,
    private val repo: SearchRepository
) : BaseViewModel() {

    private val _movies = MutableLiveData(listOf<Movie>())
    val movies: LiveData<List<Movie>> = _movies

    fun fetchMovies() {
        repo.getMovies("au")
            .ioToMainSubscribe(
                onSubscribe = { setLoading(true) },
                onSuccess = {
                    setLoading(false)
                    if (it.resultCount > 0)
                        _movies.value = it.results
                    else
                        setError(context.getString(R.string.error_empty))
                },
                onError = {
                    setLoading(false)
                    handleException(it) { error ->
                        setError(error ?: context.getString(R.string.error_generic))
                    }
                }
            )
    }

}
