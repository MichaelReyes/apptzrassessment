package com.technologies.apptzrassessment.feature.dashboard.movies

import android.content.Intent
import android.os.Bundle
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import com.preference.PowerPreference
import com.technologies.apptzrassessment.R
import com.technologies.apptzrassessment.core.base.BaseFragment
import com.technologies.apptzrassessment.core.base.BaseViewModel
import com.technologies.apptzrassessment.core.data.movie.Movie
import com.technologies.apptzrassessment.core.extension.fromJson
import com.technologies.apptzrassessment.core.extension.goToActivity
import com.technologies.apptzrassessment.core.extension.observe
import com.technologies.apptzrassessment.core.extension.showConfirmationMaterialDialog
import com.technologies.apptzrassessment.core.utils.Constants
import com.technologies.apptzrassessment.databinding.FragmentMoviesBinding
import com.technologies.apptzrassessment.feature.dashboard.details.MovieDetailsActivity
import com.technologies.apptzrassessment.feature.dashboard.details.MovieDetailsFragment
import kotlinx.android.synthetic.main.list_movies.*
import javax.inject.Inject

class MoviesFragment : BaseFragment<FragmentMoviesBinding>() {

    private val viewModel: MoviesViewModel by viewModels { viewModelFactory }

    @Inject
    lateinit var adapter: MoviesAdapter

    private var twoPane = false

    override val layoutRes = R.layout.fragment_movies

    override fun getViewModel(): BaseViewModel? = viewModel

    override fun onCreated(savedInstance: Bundle?) {

        twoPane = baseView.findViewById<ViewGroup>(R.id.nav_host_fragment) != null

        initViews()
        initObserver()
    }

    private fun initObserver() {
        viewModel.apply {
            fetchMovies()
            observe(movies, ::handleMovies)
        }

        binding.lifecycleOwner = this
        binding.viewModel = viewModel
    }

    private fun initViews() {
        adapter.twoPane = twoPane
        rvMovies.adapter = adapter
        adapter.clickListener = {
            if (twoPane) {
                navigateToMovieDetailsFragment(it)
            } else {
                openMovieDetailsActivity(it)
            }
        }
    }

    private fun handleMovies(movies: List<Movie>?) {
        movies?.apply {
            adapter.collection = this

            if (this.isNotEmpty())
                PowerPreference.getDefaultFile().getString(Constants.PREF_KEY_DETAILS_VIEWING, null)
                    ?.apply {
                        val movie: Movie = gson.fromJson(this)
                        if (twoPane) {
                            navigateToMovieDetailsFragment(movie)
                        } else {
                            context?.showConfirmationMaterialDialog(
                                "Last Activity",
                                "We have detected that you're currently viewing ${movie.trackName} before you left, would you like to view this again?",
                                "Yes",
                                "No",
                                positiveCallback = { openMovieDetailsActivity(movie) }
                            )
                        }
                    }
        }
    }

    private fun navigateToMovieDetailsFragment(movie: Movie) {
        val navHostFragment =
            childFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment

        navHostFragment.navController.navigate(
            R.id.movieDetailsFragment,
            bundleOf(Pair(MovieDetailsFragment.ARGS_MOVIE, gson.toJson(movie)))
        )
    }

    private fun openMovieDetailsActivity(movie: Movie) {
        goToActivity(
            MovieDetailsActivity::class.java,
            false,
            bundleOf(Pair(MovieDetailsFragment.ARGS_MOVIE, gson.toJson(movie))),
            true,
            MovieDetailsActivity.ACTIVITY_CODE
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == MovieDetailsActivity.ACTIVITY_CODE) {
            PowerPreference.getDefaultFile().remove(Constants.PREF_KEY_DETAILS_VIEWING)
        }
    }
}