package com.technologies.apptzrassessment.feature.dashboard.details

import android.os.Bundle
import com.technologies.apptzrassessment.R
import com.technologies.apptzrassessment.core.base.BaseActivity
import com.technologies.apptzrassessment.databinding.ActivityMovieDetailsBinding
import kotlinx.android.synthetic.main.activity_movie_details.*

class MovieDetailsActivity : BaseActivity<ActivityMovieDetailsBinding>() {
    override val layoutRes = R.layout.activity_movie_details

    override fun onCreated(instance: Bundle?) {
        setSupportActionBar(toolbar)
    }

    companion object{
        const val ACTIVITY_CODE = 1234
    }
}