package com.technologies.apptzrassessment.feature.dashboard.movies

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.technologies.apptzrassessment.R
import com.technologies.apptzrassessment.core.data.movie.Movie
import com.technologies.apptzrassessment.core.utils.AutoUpdatableAdapter
import com.technologies.apptzrassessment.databinding.ItemMovieBinding
import com.technologies.apptzrassessment.databinding.ItemMovieLandBinding
import javax.inject.Inject
import kotlin.properties.Delegates

class MoviesAdapter @Inject constructor() : RecyclerView.Adapter<MoviesAdapter.Holder>(),
    AutoUpdatableAdapter, MovieItemHandler {

    internal var collection: List<Movie> by Delegates.observable(emptyList()) { prop, old, new ->
        autoNotify(old, new) { o, n -> o.trackId == n.collectionId }
    }

    internal var twoPane: Boolean = false
    internal var clickListener: (Movie) -> Unit = { _ -> }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder =
        if (twoPane)
            Holder.from<ItemMovieLandBinding>(parent, R.layout.item_movie_land)
        else
            Holder.from<ItemMovieBinding>(parent, R.layout.item_movie)


    override fun getItemCount(): Int = collection.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.binding.apply {
            when (this) {
                is ItemMovieBinding -> {
                    item = collection[position]
                    handler = this@MoviesAdapter
                }
                is ItemMovieLandBinding -> {
                    item = collection[position]
                    handler = this@MoviesAdapter
                }
            }
            executePendingBindings()
        }

    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun select(movie: Movie) {
        collection.filter { it.trackId != movie.trackId }.map { it.selected = false }
        collection.singleOrNull { it.trackId == movie.trackId }?.selected = true
        notifyDataSetChanged()

        clickListener(movie)
    }

    class Holder(val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {

        companion object {
            fun <T : ViewDataBinding> from(parent: ViewGroup, layout: Int): Holder {
                val inflater = LayoutInflater.from(parent.context)
                val binding = DataBindingUtil.inflate<T>(
                    inflater,
                    layout,
                    parent,
                    false
                )
                return Holder(
                    binding as T
                )
            }
        }
    }
}
