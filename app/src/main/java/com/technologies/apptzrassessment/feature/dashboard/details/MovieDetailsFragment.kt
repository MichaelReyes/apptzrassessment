package com.technologies.apptzrassessment.feature.dashboard.details

import android.os.Bundle
import androidx.fragment.app.viewModels
import com.google.android.exoplayer2.ui.PlayerView
import com.preference.PowerPreference
import com.technologies.apptzrassessment.R
import com.technologies.apptzrassessment.core.base.BaseActivity
import com.technologies.apptzrassessment.core.base.BaseVideoPlayerFragment
import com.technologies.apptzrassessment.core.base.BaseViewModel
import com.technologies.apptzrassessment.core.data.movie.Movie
import com.technologies.apptzrassessment.core.extension.fromJson
import com.technologies.apptzrassessment.core.extension.observe
import com.technologies.apptzrassessment.core.utils.Constants
import com.technologies.apptzrassessment.databinding.FragmentMovieDetailsBinding
import kotlinx.android.synthetic.main.fragment_movie_details.*

class MovieDetailsFragment : BaseVideoPlayerFragment<FragmentMovieDetailsBinding>() {

    private val viewModel: MovieDetailsViewModel by viewModels { viewModelFactory }

    override val layoutRes = R.layout.fragment_movie_details

    override fun getViewModel(): BaseViewModel? = viewModel

    override fun getPlayerView(): PlayerView? = movie_detail_pv_trailer

    override fun onCreated(savedInstance: Bundle?) {
        initObserver()
        checkArgs()
    }

    private fun initObserver() {
        viewModel.apply {
            observe(movie, ::handleMovie)
        }
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
    }

    private fun handleMovie(movie: Movie?) {
        movie?.apply {
            if (previewUrl.isNotEmpty()) {
                playTrack("https://multiplatform-f.akamaihd.net/i/multi/will/bunny/big_buck_bunny_,640x360_400,640x360_700,640x360_1000,950x540_1500,.f4v.csmil/master.m3u8")
            }

            if (activity is MovieDetailsActivity)
                (activity as MovieDetailsActivity).setToolbar(
                    show = true,
                    showBackButton = true,
                    title = trackName
                )

            PowerPreference.getDefaultFile()
                .putString(Constants.PREF_KEY_DETAILS_VIEWING, gson.toJson(this))
        }
    }

    private fun checkArgs() {
        if (activity?.intent?.hasExtra(ARGS_MOVIE) == true) {
            viewModel.setMovie(
                gson.fromJson(
                    activity?.intent?.extras?.getString(ARGS_MOVIE, "") ?: ""
                )
            )
        } else if (arguments?.containsKey(ARGS_MOVIE) == true) {
            viewModel.setMovie(gson.fromJson(arguments?.getString(ARGS_MOVIE, "") ?: ""))
        }
    }


    companion object {
        const val ARGS_MOVIE = "_args_movie"
    }
}