package com.technologies.apptzrassessment.feature.dashboard.movies

import com.technologies.apptzrassessment.core.data.movie.Movie

interface MovieItemHandler {

    fun select(movie: Movie)

}