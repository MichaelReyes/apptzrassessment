package com.technologies.apptzrassessment.feature.dashboard.details

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.technologies.apptzrassessment.R
import com.technologies.apptzrassessment.core.base.BaseViewModel
import com.technologies.apptzrassessment.core.data.movie.Movie
import com.technologies.apptzrassessment.core.extension.ioToMainSubscribe
import com.technologies.apptzrassessment.core.network.repository.SearchRepository
import javax.inject.Inject

class MovieDetailsViewModel
@Inject constructor() : BaseViewModel() {

    private val _movie = MutableLiveData<Movie>()
    val movie: LiveData<Movie> = _movie
    fun setMovie(movie: Movie){
        _movie.value = movie
    }

}
