package com.technologies.apptzrassessment.feature.dashboard

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.technologies.apptzrassessment.R
import com.technologies.apptzrassessment.core.base.BaseActivity
import com.technologies.apptzrassessment.databinding.ActivityDashboardBinding
import kotlinx.android.synthetic.main.activity_dashboard.*

class DashboardActivity : BaseActivity<ActivityDashboardBinding>() {
    override val layoutRes = R.layout.activity_dashboard

    override fun onCreated(instance: Bundle?) {
        setSupportActionBar(toolbar)
    }
}