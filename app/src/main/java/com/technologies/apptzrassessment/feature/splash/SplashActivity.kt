package com.technologies.apptzrassessment.feature.splash

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.technologies.apptzrassessment.R
import com.technologies.apptzrassessment.core.base.BaseActivity
import com.technologies.apptzrassessment.core.extension.goToActivity
import com.technologies.apptzrassessment.databinding.ActivitySplashBinding
import com.technologies.apptzrassessment.feature.dashboard.DashboardActivity
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : BaseActivity<ActivitySplashBinding>() {
    override val layoutRes = R.layout.activity_splash

    override fun onCreated(instance: Bundle?) {
        initViews()
    }

    private fun initViews() {
        splash_animationView.addAnimatorUpdateListener {
            if ((it.animatedValue as Float * 100).toInt() == 90){
                goToActivity(DashboardActivity::class.java, true, null)
            }
        }
    }
}